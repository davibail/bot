#!/bin/bash

source /etc/os-release
osname=$(echo $PRETTY_NAME | cut -d' ' -f1)

case $osname in

  Debian)
    echo -n "Debian"
    sudo apt-get install python3 python3-pip python3-wheel swig libpulse-dev libasound2-dev curl
    ;;

  Fedora)
    echo -n "Fedora"
    sudo dnf install python3 python3-pip python3-wheel swig pulseaudio-libs-devel alsa-lib-devel curl
    ;;

  *)
    echo -n "unknown os"
    ;;
esac

pip3 install --upgrade playsound gTTS vosk
curl -OJ "https://alphacephei.com/vosk/models/vosk-model-fr-0.22.zip"
unzip "vosk-model-fr-0.22.zip"
mv vosk-model-fr-0.22 model


