#!/usr/bin/env python3

from datetime import datetime
import json
import random
import urllib.request


class AI:
    def __init__ (self, name, language):
        self.name=name
        with open("choices.json", "r", encoding='utf-8') as f:
            self.choices = json.loads(f.read())
        with open("language/"+language+"/translations.json", "r", encoding='utf-8') as f:
            self.translations = json.loads(f.read())
        with open("language/"+language+"/sentences.json", "r", encoding='utf-8') as f:
            self.sentences = json.loads(f.read())
        self.query=""
        self.answer=""
        self.nb_silences=0
        self.nb_untaged=0
        self.debug=0
    

    def setDebug(self,level):
        self.debug=level

    
    def debugPrint(self,text,level=1):
        if level <= self.debug:
            print(text)
    

    def addToAnswer(self,text):
        self.answer+=text+". "


    def getLocation(self):
        with urllib.request.urlopen("https://geolocation-db.com/json") as url:
            data = json.loads(url.read().decode())
        text=self.getDefaultResponse("location")
        text=text.replace("%city%",data["city"])
        text=text.replace("%country%",data["country_name"])
        return text


    def getAge(self):
        timestamp = 1649941200
        then = datetime.fromtimestamp(timestamp)
        now = datetime.now()
        duration = now - then
        duration_in_s = duration.total_seconds()
        self.debugPrint(duration,2)
        days    = divmod(duration_in_s, 86400)        # Get days (without [0]!)
        hours   = divmod(days[1], 3600)               # Use remainder of days to calc hours
        minutes = divmod(hours[1], 60)                # Use remainder of hours to calc minutes
        seconds = divmod(minutes[1], 1)               # Use remainder of minutes to calc seconds
        text=self.getDefaultResponse("age")
        text=text.replace("%days%",str(int(days[0])))
        text=text.replace("%hours%",str(int(hours[0])))
        text=text.replace("%minutes%",str(int(minutes[0])))
        text=text.replace("%seconds%",str(int(seconds[0])))
        return text
    

    def getDefaultResponse(self,key):
        index=random.randint(0, len(self.sentences[key])-1)
        print(len(self.sentences[key]))
        print(index)
        text=self.sentences[key][index]
        return text


    def getName(self):
        text=""
        if not self.getName:
            text=self.getDefaultResponse("I don't know")
        else:
            text=self.getDefaultResponse("name")
            text=text.replace("%name%",self.name)
        return text


    def getGreetings(self):
        text=""
        time = datetime.now()
        if time.hour >= 0 and time.hour <= 11:
            print("good morning")
            text=self.getDefaultResponse("good morning")
        elif time.hour >= 12 and time.hour <= 17:
            print("good afternoon")
            text=self.getDefaultResponse("good afternoon")
        elif time.hour >= 18 and time.hour <= 23:
            print("good evening")
            text=self.getDefaultResponse("good evening")
        return text
    

    def getTime(self):
        time = datetime.now()
        text=self.getDefaultResponse("time")
        text=text.replace("%hours%",str(time.hour))
        text=text.replace("%minutes%",str(time.minute))
        text=text.replace("%seconds%",str(time.second))
        return text
    

    def getRandomQuestion(self):
        text=""
        time = datetime.now()
        if time.hour >= 0 and time.hour <= 11:
            print("good morning")
            text=self.getDefaultResponse("what are the plans?")
        elif time.hour >= 12 and time.hour <= 17:
            print("good afternoon")
            text=self.getDefaultResponse("how was the morning?")
        elif time.hour >= 18 and time.hour <= 23:
            print("good evening")
            text=self.getDefaultResponse("how was your day?")
        return text
        
    
    def runCommand(self, command):
        if command=="getName":
            return self.getName()
        elif command=="getAge":
            return self.getAge()
        elif command=="getTime":
            return self.getTime()
        elif command=="getLocation":
            return self.getLocation()
        elif command=="getGreetings":
            return self.getGreetings()
        return self.getDefaultResponse("I don't know")
    

    def splitQuery(self, query):
        specialChars = "-'!?"
        for specialChar in specialChars:
            query = query.replace(specialChar," ")
        words = query.split()

        index=0
        for word in words:
            if word in self.translations:
                words[index] = self.translations[word]
            index+=1
        return words


    def process(self,query):
        print("processing "+query)
        if query.strip():
            self.answer=""
            words = self.splitQuery(query)

            if self.name in query or self.nb_untaged < 3:
                self.nb_silences = 0
                node=self.choices
                name_found=False
                for word in words:
                    self.debugPrint(word)
                    self.debugPrint(node)
                    if word == self.name:
                        name_found=True
                        continue
                    if word in node:
                        node = node[word]
                
                if name_found:
                    self.debugPrint(node)
                    if "$default" in node:
                        self.addToAnswer(self.getDefaultResponse(node["$default"]))
                    elif "$command" in node:
                        self.addToAnswer(self.runCommand(node["$command"]))
                    else:
                        self.addToAnswer(self.getDefaultResponse("I don't know"))
            
            if not name_found:
                self.nb_untaged+=1
        
        self.nb_silences += 1
        
        return self.answer