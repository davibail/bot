# Bot
The project aim to build an autonomous bot able to understand and respond

### Install

#### Linux
sudo apt-get install python-pyaudio python3-pyaudio
pip3 install pyaudio gTTS playsound SpeechRecognition pydub

#### MacOs
brew install portaudio
pip3 install pyaudio gTTS playsound SpeechRecognition pydub

#### Windows
pip3 install pipwin
pipwin install pyaudio gTTS playsound SpeechRecognition pydub

### Configuration
None for now

### Launching
No argument for now